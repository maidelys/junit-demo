/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */

package condition;

import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.*;
import java.time.DayOfWeek;

/**
 * @see DisabledOnDayOfWeek
 * @see org.junit.jupiter.api.Disabled
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ExtendWith(EnabledOnDayOfWeekCondition.class)
public @interface EnabledOnDayOfWeek {

	DayOfWeek[] value();

}
